<?php
/**
 * @file
 * kalnins_drupal_prd2.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function kalnins_drupal_prd2_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function kalnins_drupal_prd2_node_info() {
  $items = array(
    'produkts' => array(
      'name' => t('Produkts'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
