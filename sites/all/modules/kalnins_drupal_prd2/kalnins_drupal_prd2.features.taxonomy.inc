<?php
/**
 * @file
 * kalnins_drupal_prd2.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function kalnins_drupal_prd2_taxonomy_default_vocabularies() {
  return array(
    'produktu_katalogs' => array(
      'name' => 'Produktu katalogs',
      'machine_name' => 'produktu_katalogs',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
