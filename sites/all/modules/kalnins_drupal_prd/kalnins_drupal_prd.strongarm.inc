<?php
/**
 * @file
 * kalnins_drupal_prd.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function kalnins_drupal_prd_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_mail';
  $strongarm->value = 'rihards.kalnins@va.lv';
  $export['site_mail'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_name';
  $strongarm->value = 'Kalniņš Drupal PrD';
  $export['site_name'] = $strongarm;

  return $export;
}
